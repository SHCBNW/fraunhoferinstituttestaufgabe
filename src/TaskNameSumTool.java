import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

import static java.lang.System.currentTimeMillis;

public class TaskNameSumTool {

    private static String[] nameList;
    private static long[] sumList;
    private static long[] multiList;

    public static void main(String[] args) {
        long start = currentTimeMillis();
        run();
        print();
        long stop = currentTimeMillis();
        System.out.println(nameList.length + " items processed within " + (stop - start) + " ms.");
    }

    private static void run() {
        nameList = getNamesList();
        sumList = new long[nameList.length];
        multiList = new long[nameList.length];
        sortList(nameList);
        for (int i = 0; i < nameList.length; i++) {
            sumList[i] = sumName(nameList[i]);
            multiList[i] = sumList[i] * (i + 1);
        }
    }

    private static String[] getNamesList() {
        String rawData = readFile("src/names.txt");
        if (rawData != null) rawData = rawData.replaceAll(",\"", "").substring(1);
        //noinspection ConstantConditions
        return rawData.split("\"");
    }

    @SuppressWarnings("SameParameterValue")
    private static String readFile(String path) {
        String line;
        try {
            FileReader fileReader = new FileReader(path);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            if ((line = bufferedReader.readLine()) != null) {
                bufferedReader.close();
                return line;
            }
        } catch (FileNotFoundException ex) {
            System.out.println("File not found: '" + path + "'");
        } catch (IOException ex) {
            System.out.println("Unable to read file: '" + path + "'");
        }
        return null;
    }

    private static void sortList(String[] list) {
        Arrays.sort(list);
    }

    private static long sumName(String name) {
        long sum = 0;
        for (int i = 0; i < name.length(); i++)
            sum += charToInteger(name.charAt(i));
        return sum;
    }

    private static int charToInteger(char c) {
        String s = String.valueOf(c).toUpperCase();
        return "ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(s) + 1; // Starts counting with 1
    }

    private static void print() {
        for (int i = 0; i < nameList.length; i++)
            System.out.println(nameList[i] + ": " + (i + 1) + " x " + sumList[i] + " = " + multiList[i]);
    }

}